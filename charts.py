import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def chart_correlation():
    df = pd.read_csv("Datapoints.csv")
    fig = px.scatter(df,y="funding",x="Funding")
    fig.update_xaxes(type='linear')
    print(df["funding"].mean())
    fig.show()


df = pd.read_csv("Datapoints.csv")
# Create figure with secondary y-axis
fig = make_subplots(specs=[[{"secondary_y": True}]])
fig.update_xaxes(type='linear')

# Add traces
fig.add_trace(
    go.Scatter(y=df["funding_sum"], name="funding_sum"),
    secondary_y=False,
)
fig.add_trace(
    go.Scatter(y=df["dif 1"], name="dif 1"),
    secondary_y=True,
)
fig.add_trace(
    go.Scatter(y=df["dif 2"], name="dif 2"),
    secondary_y=True,
)
fig.add_trace(
    go.Scatter(y=df["dif 3"], name="dif 3"),
    secondary_y=True,
)
fig.add_trace(
    go.Scatter(y=df["dif 4"], name="dif 4"),
    secondary_y=True,
)
fig.add_trace(
    go.Scatter(y=df["dif 5"], name="dif 5"),
    secondary_y=True,
)
fig.add_trace(
    go.Scatter(y=df["dif 6"], name="dif 6"),
    secondary_y=True,
)
fig.add_trace(
    go.Scatter(y=df["dif 7"], name="dif 7"),
    secondary_y=True,
)

fig.update_yaxes(type='linear')



fig.write_html('ADAUSD.html')

