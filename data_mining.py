# pip install binance-connector (spot)
# pip install coś jak binance futures //to jest drugi sdk do obsługi futures (chyba gorszy)
# pip install binance-futures-connector//to jest lepszy sdk ale zacząłem używać tegi drugiego wczesniej
import time

import pandas as pd
from binance.cm_futures import CMFutures
from binance.um_futures import UMFutures
from binance.websocket.cm_futures.websocket_client import CMFuturesWebsocketClient
import logging
import plotly.express as px
import binance_d
import binance_f

from binance_f import RequestClient
# from binance_d import RequestClient
from datetime import datetime, timezone
from binance.spot import Spot

from binance.lib.utils import config_logging

from binance_f.model.constant import *
import re
from unidecode import unidecode
from decimal import Decimal
import sys
import os

from binance_futures_client import place_order_available
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

datapoints_df = pd.DataFrame()
symbol = "ETHUSD"
date = datetime(2022, 9, 23, 0, 0, 00, 0, tzinfo=timezone.utc)
interval = CandlestickInterval.HOUR8
end_time = []
end_time.append(int(datetime(2021, 3, 26, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2021, 6, 25, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2021, 9, 24, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2021, 12, 31, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2022, 3, 25, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2022, 6, 24, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2022, 9, 30, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))
end_time.append(int(datetime(2022, 12, 30, 0, 0, 00, 0, tzinfo=timezone.utc).timestamp()))


def get_historical_price(s_time, symbol, interval, limit):
    if "USDT" in symbol:
        futures_client = UMFutures()
    else:
        futures_client = CMFutures()

    result = futures_client.mark_price_klines(symbol=symbol, interval=interval,
                                              startTime=s_time * 1000, limit=limit)

    if len(result) > 0:
        open_price = Decimal(result[0].open)
    else:
        open_price = 0

    return open_price


def get_historical_prices(end_time, symbol, interval, limit):
    if "USDT" in symbol:
        futures_client = UMFutures()
    else:
        futures_client = CMFutures()
    return futures_client.mark_price_klines(symbol=symbol, interval=interval,
                                            **{"limit": limit, "endTime": end_time * 1000})


def get_historical_index_prices(end_time, symbol, interval, limit):
    if "USDT" in symbol:
        futures_client = UMFutures()
    else:
        futures_client = CMFutures()
    return futures_client.index_price_klines(symbol, interval, **{"limit": limit, "endTime": end_time * 1000})


def get_historical_funding_rates(end_time, symbol, limit):
    if "USDT" in symbol:
        futures_client = UMFutures()
    else:
        futures_client = CMFutures()
    return futures_client.funding_rate(symbol, endTime=end_time * 1000, limit=limit)


def get_historical_prices_spot(s_time, symbol, interval, limit):
    client = Spot()
    return (client.klines(symbol, interval, limit=limit, startTime=s_time * 1000))


def get_current_timestamp():
    request_client = binance_f.RequestClient()

    result = request_client.get_servertime()
    return result / 1000


def datapoints_to_file():
    global datapoints_df

    limit = 500

    symbol_perp = symbol + "_PERP"

    timestamp_stop = int(date.timestamp())
    timestamp_step = limit * 8 * 3600

    historical_prices_df = pd.DataFrame()
    funding_rates_df = pd.DataFrame()
    index_prices_df = pd.DataFrame()

    for i in range(3, -1, -1):
        result = pd.DataFrame(get_historical_prices(timestamp_stop - i * timestamp_step, symbol_perp, interval, limit))
        result = result.iloc[:, [0, 1]]
        historical_prices_df = pd.concat([historical_prices_df, result])

        result = pd.DataFrame(get_historical_index_prices(timestamp_stop - i * timestamp_step, symbol, interval, limit))
        result = result.iloc[:, [0, 1]]
        index_prices_df = pd.concat([index_prices_df, result])

        result = pd.DataFrame(
            get_historical_funding_rates(timestamp_stop + 100 - i * timestamp_step, symbol_perp, limit))
        # przesuwam timestamp o 100 sekund bo niekiedy funding rate nie wypadają równo
        result = result.iloc[:, [1, 2]]
        funding_rates_df = pd.concat([funding_rates_df, result])

    historical_prices_df = historical_prices_df.rename(columns={0: "time", 1: "mark_price"}).set_index("time")
    index_prices_df = index_prices_df.rename(columns={0: "time", 1: "index_price"}).set_index("time")
    funding_rates_df = funding_rates_df \
        .rename(columns={funding_rates_df.columns[0]: "time",
                         funding_rates_df.columns[1]: "funding_rate"})
    funding_rates_df["time"] = funding_rates_df["time"].round(-5)
    funding_rates_df = funding_rates_df.set_index("time")

    datapoints_df = pd.concat([historical_prices_df, index_prices_df, funding_rates_df], axis=1)
    datapoints_df["time"] = pd.to_datetime(datapoints_df.index, unit="ms")
    datapoints_df.insert(0, 'time', datapoints_df.pop('time'))
    datapoints_df["mark_price"] = datapoints_df["mark_price"].astype(float)
    datapoints_df["index_price"] = datapoints_df["index_price"].astype(float)
    datapoints_df["funding_rate"] = datapoints_df["funding_rate"].astype(float) * 100
    datapoints_df["funding_sum"] = datapoints_df["funding_rate"].cumsum()
    datapoints_df["mark-index"] = datapoints_df["mark_price"] - datapoints_df["index_price"]

    current_timestamp = int(get_current_timestamp())
    for timestamp in end_time:
        date_str = datetime.utcfromtimestamp(timestamp).strftime("_%y%m%d")
        stop_timestamp = min([current_timestamp, timestamp])
        result = pd.DataFrame(get_historical_prices(stop_timestamp, symbol + date_str, interval, limit))
        contract_prices_df = result.iloc[:, [0, 1]]
        contract_prices_df = contract_prices_df.rename(columns={0: "time", 1: "price" + date_str}).set_index("time")
        contract_prices_df["price" + date_str] = contract_prices_df["price" + date_str].astype(float)
        contract_prices_df["days_left" + date_str] = round(
            (timestamp * 1000 - contract_prices_df.index.astype(float).to_series()) / 1000 / 3600 / 24, 1)

        datapoints_df = pd.concat([datapoints_df, contract_prices_df], axis=1)

        datapoints_df["d" + date_str] = (datapoints_df["price" + date_str] - datapoints_df["mark_price"]) / \
                                        datapoints_df["mark_price"] * 100
        datapoints_df["d/8h" + date_str] = datapoints_df["d" + date_str] / datapoints_df["days_left" + date_str] / 3
        datapoints_df["d_change" + date_str] = datapoints_df["d" + date_str].diff()
        # -1 gdy pozycja LONG na PERP, bo pozycje long płacą pozycjom short, wtedy d jest dodatnie
        # +1 gdy SHORT na PERP, wtedy d jest ujemne
        position_sign = -datapoints_df["d" + date_str].shift(1).abs() / datapoints_df["d" + date_str].shift(1)
        datapoints_df["gain" + date_str] = (datapoints_df["funding_rate"] + datapoints_df[
            "d_change" + date_str]) * position_sign
        datapoints_df["total_gain" + date_str] = datapoints_df["gain" + date_str].cumsum()

    datapoints_df.to_csv("datapoints_" + symbol + "_" + interval + ".csv")
    pass


def plot():
    datapoints_df = pd.read_csv("datapoints_" + symbol + "_" + interval + ".csv")
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.update_xaxes(type='linear')

    fig.add_trace(
        go.Scatter(y=datapoints_df["funding_sum"], name="funding_sum"),
        secondary_y=False,
    )
    for timestamp in end_time:
        date_str = datetime.utcfromtimestamp(timestamp).strftime("_%y%m%d")
        fig.add_trace(
            go.Scatter(y=datapoints_df["d" + date_str], name="d" + date_str),
            secondary_y=True,
        )
    #     fig.add_trace(
    #         go.Scatter(y=datapoints_df["gain" + date_str], name="gain" + date_str),
    #         secondary_y=True,
    #     )
    #     fig.add_trace(
    #         go.Scatter(y=datapoints_df["total_gain" + date_str], name="total_gain" + date_str),
    #         secondary_y=False,
    #     )

    #
    fig.add_trace(
        go.Scatter(y=datapoints_df["mark-index"], name="mark-index"),
        secondary_y=True,
    )
    # fig.add_trace(
    #     go.Scatter(y=datapoints_df["mark_price"], name="mark_price"),
    #     secondary_y=False,
    # )
    fig.add_trace(
        go.Scatter(y=datapoints_df["funding_rate"], name="funding_rate"),
        secondary_y=True,
    )

    # fig.add_trace(
    #     go.Scatter(y=datapoints_df["mark_price"], name="mark_price"),
    #     secondary_y=False,
    # )
    # fig.add_trace(
    #     go.Scatter(y=datapoints_df["price_210924"], name="price_210924"),
    #     secondary_y=False,
    # )
    # fig.add_trace(
    #     go.Scatter(y=datapoints_df["index_price"], name="index_price"),
    #     secondary_y=False,
    # )

    fig.update_yaxes(type='linear')
    fig.write_html("datapoints_" + symbol + "_" + interval + ".html", auto_open=True)
    return


# datapoints_to_file()
plot()
