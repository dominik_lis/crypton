import math
from math import log10, floor
from decimal import Decimal
from binance_f import RequestClient

from binance_f.constant.test import *
from binance_f.base.printobject import *
from binance_f.model.constant import *

LEVERAGE = {
    "1INCH": 20,
    "AAVE": 50,
    "ADA": 50,
    "AKRO": 20,
    "ALGO": 20,
    "ALICE": 20,
    "ALPHA": 20,
    "ANKR": 20,
    "ATOM": 20,
    "AVAX": 20,
    "AXS": 20,
    "BAKE": 20,
    "BAL": 20,
    "BAND": 20,
    "BAT": 20,
    "BCH": 50,
    "BEL": 20,
    "BLZ": 20,
    "BNB": 50,
    "BTC": 125,
    "BTS": 20,
    "BTT": 20,
    "BZRX": 20,
    "CELR": 20,
    "CHR": 20,
    "CHZ": 20,
    "COMP": 20,
    "COTI": 20,
    "CRV": 20,
    "CTK": 20,
    "CVC": 20,
    "DASH": 20,
    "DEFI": 20,
    "DENT": 20,
    "DODO": 20,
    "DOGE": 50,
    "DOT": 50,
    "EGLD": 20,
    "ENJ": 20,
    "EOS": 50,
    "ETC": 50,
    "ETH": 75,
    "FIL": 50,
    "FLM": 20,
    "FTM": 20,
    "GRT": 20,
    "HBAR": 20,
    "HNT": 20,
    "HOT": 20,
    "ICP": 20,
    "ICX": 20,
    "IOST": 20,
    "IOTA": 20,
    "KAVA": 20,
    "KNC": 20,
    "KSM": 20,
    "LINA": 20,
    "LINK": 50,
    "LIT": 20,
    "LRC": 20,
    "LTC": 50,
    "LUNA": 20,
    "MANA": 20,
    "MATIC": 50,
    "MKR": 20,
    "MTL": 20,
    "NEAR": 20,
    "NEO": 20,
    "NKN": 20,
    "OCEAN": 20,
    "OGN": 20,
    "OMG": 20,
    "ONE": 20,
    "ONT": 20,
    "QTUM": 20,
    "REEF": 20,
    "REN": 20,
    "RLC": 20,
    "RSR": 20,
    "RUNE": 20,
    "RVN": 20,
    "SAND": 20,
    "SC": 20,
    "SFP": 20,
    "SKL": 20,
    "SNX": 20,
    "SOL": 50,
    "SRM": 20,
    "STMX": 20,
    "STORJ": 20,
    "SUSHI": 20,
    "SXP": 20,
    "THETA": 50,
    "TOMO": 20,
    "TRB": 20,
    "TRX": 50,
    "UNFI": 20,
    "UNI": 50,
    "VET": 20,
    "WAVES": 20,
    "XEM": 20,
    "XLM": 50,
    "XMR": 50,
    "XRP": 50,
    "XTZ": 20,
    "YFI": 20,
    "YFII": 20,
    "ZEC": 20,
    "ZEN": 20,
    "ZIL": 20,
    "ZRX": 20,
}


def get_available_balance():
    request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                   secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")
    result = request_client.get_balance_v2()
    return result[1].availableBalance

def round_sig(x, sig):
    return round(x, sig - int(floor(log10(abs(x)))) - 1)


def change_all_leverages():
    request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                   secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")
    for ticker in LEVERAGE:
        pair = ticker + "USDT"
        request_client.change_initial_leverage(pair, LEVERAGE[ticker])





def place_order_available(ticker, balance_fraction,trailing_stop_percent):
    pair = ticker + "USDT"

    request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                   secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")
    result = request_client.get_balance_v2()

    for element in result:
        if element.asset == "USDT":
            available_balance_USDT = element.availableBalance

    used_margin = available_balance_USDT*balance_fraction
    if ticker in LEVERAGE:
        leverage = LEVERAGE[ticker]
    else:
        leverage = 20

    quantity_usd = used_margin * leverage
    result = request_client.get_mark_price(symbol=pair)
    mark_price = result.markPrice
    precision_usd = 100
    quantity_crypto_min = precision_usd / mark_price  # how much for x USDT
    decimals_quantity = -math.floor(math.log(quantity_crypto_min, 10))
    quantity_crypto = round(quantity_usd / mark_price, decimals_quantity)
    quantity_crypto_decimal=Decimal(str(quantity_crypto))
    # qty=quantity_usd/mark_price

    result = request_client.post_order(symbol=pair,
                                       side=OrderSide.BUY,
                                       positionSide=PositionSide.LONG,
                                       # ordertype=OrderType.MARKET,
                                       ordertype=OrderType.MARKET,
                                       quantity=quantity_crypto_decimal,
                                       )
    result = request_client.post_order(symbol=pair,
                                       side=OrderSide.SELL,
                                       positionSide=PositionSide.LONG,
                                       ordertype=OrderType.TRAILING_STOP_MARKET,
                                       #reduceOnly=True,
                                       quantity=quantity_crypto_decimal,
                                       callbackRate=trailing_stop_percent,
                                       )

    return None

def sell_order(ticker, quantity_usd):
    pair = ticker + "USDT"
    request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                   secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")
    result = request_client.get_mark_price(symbol=pair)
    mark_price = result.markPrice
    precision_usd = 50
    quantity_crypto_min = precision_usd / mark_price  # how much for x USDT
    decimals_quantity = -math.floor(math.log(quantity_crypto_min, 10))
    quantity_crypto = round(quantity_usd / mark_price, decimals_quantity)
    quantity_crypto_decimal = Decimal(str(quantity_crypto))
    # qty=quantity_usd/mark_price

    result = request_client.post_order(symbol=pair,
                                       side=OrderSide.SELL,
                                       # ordertype=OrderType.MARKET,
                                       ordertype=OrderType.MARKET,
                                       quantity=quantity_crypto_decimal,
                                       )
    result = request_client.post_order(symbol=pair,
                                       side=OrderSide.BUY,
                                       ordertype=OrderType.TRAILING_STOP_MARKET,
                                       # reduceOnly=True,
                                       quantity=quantity_crypto_decimal,
                                       callbackRate=0.1,
                                       )

    return None

#change_all_leverages()
#place_order_available('ETH',0.1)