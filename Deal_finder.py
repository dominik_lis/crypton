# pip install binance-connector
import logging
import math
from datetime import datetime
import time
from decimal import Decimal
# TODO: wyjdź gdy error
import pandas as pd
from binance.error import ClientError
from binance.um_futures import UMFutures
from binance.cm_futures import CMFutures
from binance.spot import Spot as Client
from binance.websocket.cm_futures.websocket_client import CMFuturesWebsocketClient
from binance.websocket.um_futures.websocket_client import UMFuturesWebsocketClient

# noinspection SpellCheckingInspection
key = "uRaiHPIsGDOQ8ywIUD1TbSEFLqho89HX7gQLVeAReF1YPwPxmO5j4bmYY9ZuCZaX"
# noinspection SpellCheckingInspection
secret = "89bo67ByIRem6BMo0jo6auEkOlCN10oiwW4XEntdMN83bnhKovB9OlIvUmsnPbep"

MarkPrices = pd.DataFrame(columns=["Symbol", "MarkPrice"])
MS_IN_DAY = 86400000
QUANTITY = 0.1
contracts_df = pd.DataFrame()
info = pd.DataFrame()
info_spot = pd.DataFrame()

# Kiedy zmieniam
# Jeśli nowy dday po odjęciu kosztów przejścia jest większy od poprzedniego
# dday2>dday1*D2/(D2-K) K- koszt zmiany
CONTRACT_CHANGE_COST_UM = 0.16  # =4 *0,04
DDAY_DIFF_TO_CHANGE_UM = 0.035  # Co najmniej tyle, żeby zarobić - (w eth było 0,044

CONTRACT_CHANGE_COST_CM = 0.22  # (4 * 0.05)+0,1*2 (2x swap na połowie kasy + 2x przewalutowanie spot na 1/20 kasy)
DDAY_DIFF_TO_CHANGE_CM = 0.035  # zmieniam tylko, gdy nie stracę zmianie kontraktów (
LEVERAGE_CM = 20
D_TO_CLOSE = 0.01
D_TO_CHANGE = 3  # jak tyle będzie to kupuj od razu


def get_logger(
        LOG_FORMAT='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        LOG_NAME='',
        LOG_FILE_INFO='deal_finder.log',
        LOG_FILE_ERROR='deal_finder.err'):
    log = logging.getLogger(LOG_NAME)
    log_formatter = logging.Formatter(LOG_FORMAT)

    # comment this to suppress console output
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_formatter)
    log.addHandler(stream_handler)

    file_handler_info = logging.FileHandler(LOG_FILE_INFO, mode='w')
    file_handler_info.setFormatter(log_formatter)
    file_handler_info.setLevel(logging.INFO)
    log.addHandler(file_handler_info)

    file_handler_error = logging.FileHandler(LOG_FILE_ERROR, mode='w')
    file_handler_error.setFormatter(log_formatter)
    file_handler_error.setLevel(logging.ERROR)
    log.addHandler(file_handler_error)

    log.setLevel(logging.INFO)

    return log


def message_handler(message):
    global contracts_df
    if 's' in message:
        if "_PERP" in message['s']:  # Kontrakty CM mają te _perp
            pair = message['s'][0:-5]  # obcinam _perp
            contracts_df.loc[contracts_df['pair'] == pair, "price_perp"] = Decimal(message['p'])
        elif '_' in message['s']:
            contracts_df.at[message["s"], "price"] = Decimal(message['p'])
        else:
            contracts_df.loc[contracts_df['pair'] == message['s'], "price_perp"] = Decimal(message['p'])


# contract_types = ["CURRENT_QUARTER", "NEXT_QUARTER"]
def quantity_correct_spot(symbol, desired_qty):
    global contracts_df
    global info_spot
    desired_qty = Decimal(desired_qty)

    minQtyMarket = Decimal(info_spot.at[symbol, "filters"][2]["minQty"])
    stepSizeMarket = Decimal(info_spot.at[symbol, "filters"][2]["stepSize"])

    remnantDesired = (desired_qty - minQtyMarket) % stepSizeMarket
    desired_qty = desired_qty - remnantDesired
    correctQty = max(minQtyMarket, desired_qty)
    correctQty = Decimal.normalize(correctQty)
    return correctQty


def quantity_correct_um(symbol, desiredQty):
    global contracts_df
    global info
    desiredQty = Decimal(desiredQty)
    possible_price_change = Decimal("0.80")  # between perp and delivery
    minQtyMarket = Decimal(info.at[symbol, "filters"][2]["minQty"])
    stepSizeMarket = Decimal(info.at[symbol, "filters"][2]["stepSize"])

    minNotional = Decimal(info.at[symbol, "filters"][5]["notional"])

    if 'price' not in contracts_df.columns:
        return 0
    elif '_' in symbol:
        pair = symbol[0:-7]  # obcinam _220930
    else:
        pair = symbol

    marketPricePerpetual = contracts_df.loc[contracts_df['pair'] == pair, "price_perp"][0]
    if type(marketPricePerpetual) is not Decimal:
        return 0
    minQtyNotional = minNotional / (marketPricePerpetual * possible_price_change)
    remnantNotinal = (minQtyNotional - minQtyMarket) % stepSizeMarket
    minQtyNotional = minQtyNotional + stepSizeMarket - remnantNotinal

    remnantDesired = (desiredQty - minQtyMarket) % stepSizeMarket
    desiredQty = desiredQty - remnantDesired
    correctQty = max(minQtyMarket, desiredQty, minQtyNotional)
    correctQty = Decimal.normalize(correctQty)
    return correctQty


def quantity_correct_cm(symbol, desired_qty_coin):  # returns contracts to buy
    global contracts_df, cm_futures_client
    global info
    while True:
        try:
            mark_price = Decimal(cm_futures_client.mark_price(symbol=symbol)[0]["markPrice"])
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            cm_futures_client = CMFutures(key=key, secret=secret)
        else:
            break

    desired_qty_USD = Decimal(desired_qty_coin) * mark_price
    contract_size = Decimal(info.at[symbol, "contractSize"])
    desired_qty_cd = desired_qty_USD / contract_size
    correct_qty_cd = math.floor(desired_qty_cd)

    return correct_qty_cd


def get_info_spot():
    global info_spot
    client = Client(key, secret)
    result = client.exchange_info()
    info_spot = pd.DataFrame(result['symbols'])
    info_spot = info_spot.set_index("symbol").sort_index()


def get_contracts():
    global contracts_df, info, um_futures_client, cm_futures_client
    while True:
        try:
            result_um = um_futures_client.exchange_info()
            result_cm = cm_futures_client.exchange_info()
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            um_futures_client = UMFutures(key=key, secret=secret)
            cm_futures_client = CMFutures(key=key, secret=secret)
        else:
            break
    info_um = pd.DataFrame(result_um['symbols'])
    info_um["market"] = "UM"

    info_cm = pd.DataFrame(result_cm['symbols'])
    info_cm["market"] = "CM"

    info = pd.concat([info_um, info_cm])
    info = info.set_index("symbol").sort_index()
    info.insert(0, "symbol", info.index)
    logging.info("Tabela info wypełniona")
    new_contracts_df = info[(info['contractType'] != "PERPETUAL")
                            & (info['contractType'] != "")
                            & ((info['status'] == "TRADING") | (info['contractStatus'] == "TRADING"))][
        ["symbol", "pair", "market", "deliveryDate"]]
    new_contracts_df["subscribed"] = False
    temp_contracts_df = pd.concat([contracts_df, new_contracts_df])
    temp_contracts_df = temp_contracts_df[~temp_contracts_df.index.duplicated(keep='first')]

    temp_contracts_df["days_to_end"] = (temp_contracts_df["deliveryDate"].apply(
        int) - datetime.utcnow().timestamp() * 1000) / MS_IN_DAY
    temp_contracts_df["days_to_end"] = temp_contracts_df["days_to_end"].round(1)
    logging.info("Kontrakty temp przygotowane")
    contracts_df = temp_contracts_df.sort_index()
    logging.info("Tabela temp przepisana")


def subscribe_prices():
    # unsubscribe_all()
    global um_futures_websocket_client, cm_futures_websocket_client

    if contracts_df.shape[0] > 0:
        to_subscribe_df = contracts_df[contracts_df["subscribed"] != True]
        to_subscribe_df[to_subscribe_df["market"] == "UM"].apply(lambda row:
                                                                 um_futures_websocket_client.mark_price(
                                                                     symbol=row["symbol"],
                                                                     id=13,
                                                                     speed=1,
                                                                     callback=message_handler
                                                                 ),
                                                                 axis=1)
        to_subscribe_df[to_subscribe_df["market"] == "CM"].apply(lambda row:
                                                                 cm_futures_websocket_client.mark_price(
                                                                     symbol=row["symbol"],
                                                                     id=13,
                                                                     speed=1,
                                                                     callback=message_handler
                                                                 ),
                                                                 axis=1)
        pairs = to_subscribe_df.drop_duplicates(subset=["pair"])

        pairs[pairs["market"] == "UM"].apply(lambda row:
                                             um_futures_websocket_client.mark_price(
                                                 symbol=row["pair"],
                                                 id=13,
                                                 speed=1,
                                                 callback=message_handler
                                             ),
                                             axis=1)
        pairs[pairs["market"] == "CM"].apply(lambda row:
                                             cm_futures_websocket_client.mark_price(
                                                 symbol=row["pair"] + "_perp",
                                                 id=13,
                                                 speed=1,
                                                 callback=message_handler
                                             ),
                                             axis=1)

    contracts_df["price"] = None
    contracts_df["price_perp"] = None
    contracts_df["subscribed"] = True


def check_open_contracts_um():
    global um_futures_client
    contract = None
    side = None

    while True:
        try:
            response = um_futures_client.account(recvWindow=6000)
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            um_futures_client = UMFutures(key=key, secret=secret)
        else:
            break

    open_positions = []
    for position in response['positions']:
        if Decimal(position['initialMargin']) > 0:
            open_positions.append(position)
            if "_" in position["symbol"]:
                contract = position["symbol"]
                side = position["positionSide"]
    if len(open_positions) == 2 and \
            Decimal(open_positions[0]['positionAmt']) == -Decimal(open_positions[1]['positionAmt']):
        logging.info("Kontrakty UM otwarte")
    elif len(open_positions) > 0:
        logging.info("Coś jest otwarte na UM, a nie powinno")
        close_all_um()
    else:
        logging.info("Brak otwartych kontraktów UM")
        contract = None
        side = None

    return contract, side


def check_open_contracts_cm():
    global cm_futures_client
    contract = None
    quantity = None
    contract_delivery = None

    response = cm_futures_client.account(recvWindow=6000)
    open_positions = []
    for position in response['positions']:
        if Decimal(position['initialMargin']) > 0:
            open_positions.append(position)
            if "_2" in position["symbol"]:
                # contract_perp = position["symbol"]
                quantity = Decimal(position["positionAmt"])
                contract_delivery = position["symbol"]

    if len(open_positions) == 2:
        contract = contract_delivery
        logging.info(f"Kontrakty CM otwarte:{contract},Cd: {quantity}")

    elif len(open_positions) == 0:
        logging.info("Nie ma nawet hedge na CM")
    elif len(open_positions) == 1:
        logging.info("Jest tylko mały hedge na CM")

    return contract, quantity


def round_to_n(x, n):
    return round(x, -math.floor(math.log(x, 10)) + (n - 1))


def open_contracts_um(symbol_to_sell, symbol_to_buy, desired_qty):
    quantity = quantity_correct_um(symbol_to_sell, desired_qty)
    global um_futures_client

    try:
        um_futures_client.new_order(
            symbol=symbol_to_sell,
            positionSide="SHORT",
            side="SELL",
            type="MARKET",
            quantity=quantity
        )
        response = um_futures_client.new_order(
            symbol=symbol_to_buy,
            positionSide="LONG",
            side="BUY",
            type="MARKET",
            quantity=quantity,
        )
        logging.info(response)

    except ClientError as error:
        close_all_um()

        logging.error(
            "Found error. status: {}, error code: {}, error message: {}".format(
                error.status_code, error.error_code, error.error_message
            )
        )
        while 1:
            pass


def open_contracts_cm(symbol_to_sell, symbol_to_buy):
    global cm_futures_client
    if "_PERP" in symbol_to_buy:
        asset = symbol_to_buy[0:-8]  # bez USD_PERP
    else:
        asset = symbol_to_sell[0:-8]
    convert_crypto_cm(asset)

    [balance, asset] = get_balance_cm()

    desired_qty_coin = balance * LEVERAGE_CM / 2 * 98 / 100

    quantity_to_buy = quantity_correct_cm(symbol_to_buy, desired_qty_coin)  # symbol_to_buy - cena tego jest niższa
    quantity_to_sell = quantity_correct_cm(symbol_to_sell, desired_qty_coin)
    logging.info(f"Kupuję Cd: {quantity_to_buy}, Sprzedaję Cd: {quantity_to_buy}")
    if quantity_to_buy == 0 or quantity_to_sell == 0:
        logging.info("Nie mogę kupić 0 kontraktów")
        return

    try:
        cm_futures_client.new_order(
            symbol=symbol_to_sell,
            side="SELL",
            type="MARKET",
            quantity=quantity_to_sell
        )
        response = cm_futures_client.new_order(
            symbol=symbol_to_buy,
            side="BUY",
            type="MARKET",
            quantity=quantity_to_buy,
        )
        logging.info(response)
    except ClientError as error:
        close_all_cm()
        logging.error(
            "Found error. status: {}, error code: {}, error message: {}".format(
                error.status_code, error.error_code, error.error_message
            )
        )
        while 1:
            pass


def close_all_um():
    logging.info("Closing all UM")
    global um_futures_client
    while True:
        try:
            response = um_futures_client.account()
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            um_futures_client = UMFutures(key=key, secret=secret)
        else:
            break

    open_positions = []
    for position in response['positions']:
        if Decimal(position['initialMargin']) > 0:
            open_positions.append(position)
    logging.info(open_positions)
    for position in open_positions:
        if Decimal(position["positionAmt"]) < 0:
            side = "BUY"
        else:
            side = "SELL"

        try:
            response = um_futures_client.new_order(
                symbol=position["symbol"],
                positionSide=position["positionSide"],
                side=side,
                type="MARKET",
                quantity=abs(Decimal(position["positionAmt"])),
            )
            logging.info(response)
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            while 1:
                pass


def close_all_cm():
    logging.info("Closing all CM")
    global cm_futures_client
    while True:
        try:
            response = cm_futures_client.account()
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            cm_futures_client = CMFutures(key=key, secret=secret)
        else:
            break

    open_positions = []
    for position in response['positions']:
        if Decimal(position['initialMargin']) > 0:
            open_positions.append(position)
    logging.info(open_positions)
    for position in open_positions:
        if Decimal(position["positionAmt"]) < 0:
            side = "BUY"
        else:
            side = "SELL"

        try:
            response = cm_futures_client.new_order(
                symbol=position["symbol"],
                positionSide=position["positionSide"],
                side=side,
                type="MARKET",
                quantity=abs(Decimal(position["positionAmt"])),
            )
            logging.info(response)
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            while 1:
                pass


def update_prices():
    global contracts_df
    contracts_df["%d"] = (contracts_df["price"] - contracts_df["price_perp"]) / contracts_df["price_perp"] * 100
    contracts_df["%d"] = contracts_df["%d"].apply(float).round(3)
    contracts_df["%d/day"] = contracts_df["%d"] / contracts_df["days_to_end"]
    contracts_df["%d/day"] = contracts_df["%d/day"].round(3)


def try_close_contracts_um():
    global contracts_df, current_contract_um, current_side_um
    if current_side_um == "SHORT":
        side_sign = 1  # difference should be high positive to keep
    elif current_side_um == "LONG":
        side_sign = -1
    else:
        return  # no contracts open

    d = contracts_df.at[current_contract_um, "%d"] * side_sign
    if d < D_TO_CLOSE:
        close_all_um()


def try_close_contracts_cm():
    global contracts_df, current_contract_cm, quantity_cm
    if quantity_cm is None:
        return
    if quantity_cm < 0:
        side_sign = 1  # difference should be high positive to keep
    else:
        side_sign = -1

    d = contracts_df.at[current_contract_cm, "%d"] * side_sign
    if d <= D_TO_CLOSE:
        close_contracts_cm()


def close_contracts_cm():  # Tu przybliżenie, nie powinienem otwierać i zamykać takiej samej ilości TODO
    global current_contract_cm, quantity_cm, cm_futures_client
    current_contract_perpetual = current_contract_cm[0:-7] + "_PERP"  # obcinam datę
    if quantity_cm is None:
        logging.info("Brak kontraktów")
        return
    if quantity_cm < 0:
        sideDelivery = "BUY"
        sidePerpetual = "SELL"
        quantity_cm = abs(quantity_cm)

    else:
        sideDelivery = "SELL"
        sidePerpetual = "BUY"

    try:
        response = cm_futures_client.new_order(
            symbol=current_contract_cm,
            # positionSide=current_side_cm,
            side=sideDelivery,
            type="MARKET",
            quantity=quantity_cm,
        )
        logging.info(response)
        response = cm_futures_client.new_order(
            symbol=current_contract_perpetual,
            # positionSide=current_side_perpetual,
            side=sidePerpetual,
            type="MARKET",
            quantity=quantity_cm,
        )
        logging.info(response)
    except ClientError as error:
        logging.error(
            "Found error. status: {}, error code: {}, error message: {}".format(
                error.status_code, error.error_code, error.error_message
            )
        )
        while 1:
            pass


def find_best_contracts_um():
    global contracts_df

    # contracts_df["%d/day"].idxmax()
    symbol = pd.to_numeric(contracts_df[contracts_df['market'] == "UM"]["%d/day"]).abs().idxmax()
    if not pd.isna(symbol):
        dday = contracts_df.at[symbol, "%d/day"]
        d = contracts_df.at[symbol, "%d"]
    else:
        dday = None
        d = None

    return symbol, d, dday


def find_best_contracts_cm():
    global contracts_df

    # contracts_df["%d/day"].idxmax()
    symbol = pd.to_numeric(contracts_df[contracts_df['market'] == "CM"]["%d/day"]).abs().idxmax()
    if not pd.isna(symbol):
        dday = contracts_df.at[symbol, "%d/day"]
        d = contracts_df.at[symbol, "%d"]
    else:
        dday = None
        d = None

    return symbol, d, dday


def try_best_contracts_um():
    global current_contract_um, current_side_um, contracts_df

    [best_contract, best_d, best_dday] = find_best_contracts_um()
    if pd.isna(best_contract):
        return
    if not (abs(best_d) > 0):
        logging.info("Najlepszy kontrakt daje 0")
        return

    if current_contract_um is not None:
        current_dday = contracts_df.at[current_contract_um, "%d/day"]
        current_d = contracts_df.at[current_contract_um, "%d"]
        logging.info(f"Obecny kontrakt UM D: {current_d}, Dday: {current_dday}")
    else:
        current_dday = 0
        current_d = 0

    if best_contract == current_contract_um:
        logging.info("To najlepszy kontrakt UM")
        return

    if pd.isna(current_dday) or pd.isna(current_d):
        return
    best_dday_nett = abs(best_dday) * (abs(best_d) - CONTRACT_CHANGE_COST_UM) / abs(best_d)
    logging.info(f"Jest lepszy kontrakt UM: {best_contract}, D: {best_d}, Dday: {best_dday}")
    logging.info(f"Dday po kosztach: {best_dday_nett.round(3)}")
    # zmieniam gdy coś można zarobić dużo więcej niż obecnie
    # albo gdy są radykalne skoki na początku, które pewnie szybko spadną
    dday_diff_nett = best_dday_nett - abs(current_dday)
    if dday_diff_nett >= DDAY_DIFF_TO_CHANGE_UM \
            or ((abs(best_d) >= D_TO_CHANGE) and dday_diff_nett > 0):
        logging.info("Kupuję UM")
        if best_d > 0:
            symbol_to_sell = best_contract
            symbol_to_buy = contracts_df.at[best_contract, "pair"]
        else:
            symbol_to_sell = contracts_df.at[best_contract, "pair"]
            symbol_to_buy = best_contract

        open_contracts_um(symbol_to_sell, symbol_to_buy, QUANTITY)


def try_best_contracts_cm():
    global current_contract_cm, quantity_cm, contracts_df

    [best_contract, best_d, best_dday] = find_best_contracts_cm()
    if pd.isna(best_contract):
        return
    if not (abs(best_d) > 0):
        logging.info("Najlepszy kontrakt daje 0")
        return

    if current_contract_cm is not None:
        current_dday = contracts_df.at[current_contract_cm, "%d/day"]
        current_d = contracts_df.at[current_contract_cm, "%d"]
        logging.info(f"Obecny kontrakt CM D: {current_d}, Dday: {current_dday}")
    else:
        current_dday = 0
        current_d = 0

    if best_contract == current_contract_cm:
        logging.info("To najlepszy kontrakt CM")
        return

    if pd.isna(current_dday) or pd.isna(current_d):
        return

    best_dday_nett = abs(best_dday) * (abs(best_d) - CONTRACT_CHANGE_COST_CM) / abs(best_d)

    logging.info(f"Jest lepszy kontrakt CM: {best_contract}, D: {best_d}, Dday: {best_dday}")
    logging.info(f"Dday po kosztach: {best_dday_nett.round(3)}")

    # zmieniam gdy coś można zarobić dożo więcej niż obecnie
    # albo gdy są radykalne skoki na początku, które pewnie szybko spadną
    dday_diff_nett = best_dday_nett - abs(current_dday)
    if dday_diff_nett >= DDAY_DIFF_TO_CHANGE_CM \
            or ((abs(best_d) >= D_TO_CHANGE) and dday_diff_nett > 0):
        logging.info("Kupuję CM")
        if best_d > 0:
            symbol_to_sell = best_contract
            symbol_to_buy = contracts_df.at[best_contract, "pair"] + "_PERP"
        else:
            symbol_to_sell = contracts_df.at[best_contract, "pair"] + "_PERP"
            symbol_to_buy = best_contract
        logging.info(f"Sprzedaj: {symbol_to_sell}, Kup: {symbol_to_buy}")
        open_contracts_cm(symbol_to_sell, symbol_to_buy)


def get_balance_cm():
    global cm_futures_client
    while True:
        try:
            response = cm_futures_client.balance()
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            cm_futures_client = CMFutures(key=key, secret=secret)
        else:
            break

    balance_df = pd.DataFrame(response)
    balance_df = balance_df.set_index("asset")
    balance_df["availableBalance"] = balance_df["availableBalance"].apply(Decimal)
    balance = balance_df[balance_df["availableBalance"] > 0]["availableBalance"][0]
    asset = balance_df[balance_df["availableBalance"] > 0]["availableBalance"].index[0]
    return balance, asset


def convert_crypto_cm(to_asset):
    global cm_futures_client
    [balance, asset] = get_balance_cm()
    if asset == to_asset:
        return
    close_all_cm()
    [balance, asset] = get_balance_cm()

    client = Client(key, secret)
    while True:
        try:
            client.futures_transfer(asset=asset, amount=balance, type=4)
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            client = Client(key, secret)
        else:
            break

    if asset != "BTC":
        response = client.new_order(symbol=asset + "BTC", side="SELL", type="MARKET",
                                    quantity=quantity_correct_spot(asset + "BTC", balance))
        balance = Decimal(response["cummulativeQuoteQty"]) - Decimal(response["fills"][0]["commission"])
    if to_asset != "BTC":
        response = client.new_order(symbol=to_asset + "BTC", side="BUY", type="MARKET", quoteOrderQty=balance)
        balance = Decimal(response["executedQty"]) - Decimal(response["fills"][0]["commission"])
    while True:
        try:
            client.futures_transfer(asset=to_asset, amount=balance, type=3)
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            client = Client(key, secret)
        else:
            break
    symbol = to_asset + "USD_PERP"
    # mark_price = cm_futures_client.mark_price(symbol=symbol)[0]["markPrice"]

    quantity = quantity_correct_cm(symbol, balance)
    logging.info(f"Kupuję na mały hedge Cd:{quantity}")
    if quantity == 0:
        logging.info("Nie mogę kupić 0 kontraktów na mały hedge")
        return
    while True:
        try:
            cm_futures_client.new_order(symbol=symbol, side="SELL", type="MARKET",
                                        quantity=quantity)
        except ClientError as error:
            logging.error(
                "Found error. status: {}, error code: {}, error message: {}".format(
                    error.status_code, error.error_code, error.error_message
                )
            )
            time.sleep(30)
            cm_futures_client = CMFutures(key=key, secret=secret)
        else:
            break


my_logger = get_logger()

my_logger.info('This is an INFO message')
my_logger.warning('This is a WARNING message')
my_logger.error('This is an ERROR message')
um_futures_websocket_client = UMFuturesWebsocketClient()
cm_futures_websocket_client = CMFuturesWebsocketClient()
um_futures_websocket_client.start()
cm_futures_websocket_client.start()
prescaler = 0
while 1:
    prescaler %= 12
    if prescaler == 0:
        cm_futures_client = CMFutures(key=key, secret=secret)
        um_futures_client = UMFutures(key=key, secret=secret)
        logging.info("Pobieram i subskrybuję najnowsze kontrakty")
        get_info_spot()
        logging.info("Spot info pobrane")
        get_contracts()
        logging.info("Kontrakty pobrane")
        subscribe_prices()

    time.sleep(10)
    prescaler += 1

    update_prices()
    logging.info(f"Tabela z kontraktami:\n{contracts_df}")
    [current_contract_um, current_side_um] = check_open_contracts_um()
    [current_contract_cm, quantity_cm] = check_open_contracts_cm()
    try_close_contracts_um()
    try_close_contracts_cm()
    try_best_contracts_um()
    try_best_contracts_cm()
