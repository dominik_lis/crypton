import twitter
import time
from binance_f import RequestClient
from google.cloud import language_v1

from binance_f.model.constant import *
import re
from unidecode import unidecode
from decimal import Decimal
import sys
import os

from binance_futures_client import place_order_available

# Set environment variables
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'pythonbotapi-315219-d0dbec12d0fe.json'
api = twitter.Api(consumer_key="TJEMVDXzsmxexGwUMZw4DrPOr",
                  consumer_secret="Q10mQTjjLH168L0iYfL9LYHisUYl6LupQYICnvNCKyVhBdkcSc",
                  access_token_key="1397622006137475077-eyWQMSmANO7t64wIxJz4ri4iYwNnpW",
                  access_token_secret="UwQYs5gABVMND5GRvxGyBMdLGKUTKzOXLtGRcc6Ah3zP6")
USERS = ['1397622006137475077',
         '1397622006137475076']
# statuses=api.GetStreamSample()
ORDER_BALANCE_FRACTION = 0.01
TRAILING_STOP_PERCENT = 0.1
MIN_AGE_S = 2
MAX_AGE_S = 1860  # czas w sekundach zanim zostanąsprawdzone różnice kursowe
MAX_AGE_S_TO_BUY = 180
MIN_RETWEETS_PER_S_MAIN = 1.5  # 0.6
MIN_RETWEETS_PER_S_ALT = 0.8 # 0.3
MIN_RETWEETS_PER_S_MAIN_TO_BUY = 2  # 0.9 #1.5
MIN_RETWEETS_PER_S_ALT_TO_BUY = 1  # 0.6 #1
blacklist = {"giveaway", " rt", "retweet", "airdrop", "giving away", "follow", "nft", "post ","comment","task"}
ticker_list = {
    r"\bAlgorand\b": "ALGO",
    r"\bAlpha Finance Lab\b": "ALPHA",
    r"\bAnkr\b": "ANKR",
    r"\bAxie Infinity\b": "AXS",
    r"\bBakeryToken\b": "BAKE",
    r"\bBand Protocol\b": "BAND",
    r"\bBasic Attention Token\b": "BAT",
    r"\bBella Protocol\b": "BEL",
    r"\bBinance Coin\b": "BNB",
    r"\bBitcoin Cash\b": "BCH",
    r"\bBitcoin\b": "BTC",
    r"\bBitShares\b": "BTS",
    r"\bBluzelle\b": "BLZ",
    r"\bbZx Protocol\b": "BZRX",
    r"\bCardano\b": "ADA",
    r"\bCeler Network\b": "CELR",
    r"\bCertiK\b": "CTK",
    r"\bChainlink\b": "LINK",
    r"\bChiliz\b": "CHZ",
    r"\bChromia\b": "CHR",
    r"\bCOTI\b": "COTI",
    r"\bCurve DAO Token\b": "CRV",
    r"\bDecentraland\b": "MANA",
    r"\bDFI.Money\b": "YFII",
    r"\bDogecoin\b": "DOGE",
    r"\bElrond\b": "EGLD",
    r"\bEnjin Coin\b": "ENJ",
    r"\bEthereum Classic\b": "ETC",
    r"\bEthereum\b": "ETH",
    r"\bFilecoin\b": "FIL",
    # r"\bFlamingo\b": "FLM",
    r"\bHedera Hashgraph\b": "HBAR",
    r"\bHorizen\b": "ZEN",

    r"\biExec RLC\b": "RLC",
    r"\bInternet Computer\b": "ICP",
    r"\bIOST\b": "IOST",
    r"\bIOTA\b": "IOTA",
    r"\bKava.io\b": "KAVA",
    r"\bKusama\b": "KSM",
    r"\bKyber Network Crystal Legacy\b": "KNC",
    # r"\bLinear\b": "LINA",
    r"\bLitecoin\b": "LTC",
    r"\bLitentry\b": "LIT",
    r"\bLoopring\b": "LRC",
    r"\bMonero\b": "XMR",
    r"\bMyNeighborAlice\b": "ALICE",
    r"\bNEAR Protocol\b": "NEAR",
    #    r"\bNEM\b": "XEM",
    r"\bOcean Protocol\b": "OCEAN",
    r"\bOMG Network\b": "OMG",
    r"\bOrigin Protocol\b": "OGN",
    r"\bPolkadot\b": "DOT",
    r"\bQtum\b": "QTUM",
    r"\bRavencoin\b": "RVN",
    r"\bSafePal\b": "SFP",
    r"\bSiacoin\b": "SC",
    r"\bSKALE Network\b": "SKL",
    r"\bSolana\b": "SOL",
    r"\bStorj\b": "STORJ",
    r"\bStormX\b": "STMX",
    r"\bSushiSwap\b": "SUSHI",
    r"\bSynthetix\b": "SNX",
    r"\bTellor\b": "TRB",
    r"\bTezos\b": "XTZ",
    r"\bTHORChain\b": "RUNE",
    r"\bTomoChain\b": "TOMO",
    r"\bTRON\b": "TRX",
    r"\bUnifi Protocol DAO\b": "UNFI",
    r"\bUniswap\b": "UNI",
    r"\bVeChain\b": "VET",
    r"\bXRP\b": "XRP",
    r"\byearn.finance\b": "YFI",
    r"\bZcash\b": "ZEC",
    r"\bZilliqa\b": "ZIL",
    r"\$1INCH\b": "1INCH",
    r"\$AAVE\b": "AAVE",
    r"\$AKRO\b": "AKRO",
    r"\$ATOM\b": "ATOM",
    r"\$AVAX\b": "AVAX",
    r"\$BAL\b": "BAL",
    r"\$BTT\b": "BTT",
    r"\$COMP\b": "COMP",
    r"\$CVC\b": "CVC",
    r"\$DASH\b": "DASH",
    r"\$DEFI\b": "DEFI",
    r"\$DENT\b": "DENT",
    r"\$DODO\b": "DODO",
    r"\$EOS\b": "EOS",
    r"\$FTM\b": "FTM",
    r"\$GRT\b": "GRT",
    r"\$HNT\b": "HNT",
    r"\$HOT\b": "HOT",
    r"\$LUNA\b": "LUNA",
    r"\$MATIC\b": "MATIC",
    r"\$MKR\b": "MKR",
    r"\$MTL\b": "MTL",
    r"\$NEO\b": "NEO",
    r"\$NKN\b": "NKN",
    r"\$ONE\b": "ONE",
    r"\$ONT\b": "ONT",
    r"\$REEF\b": "REEF",
    r"\$REN\b": "REN",
    r"\$RSR\b": "RSR",
    r"\$SAND\b": "SAND",
    r"\$SRM\b": "SRM",
    r"\$SXP\b": "SXP",
    r"\$THETA\b": "THETA",
    r"\$XLM\b": "XLM",
    r"\$ZRX\b": "ZRX",
    r"\$ALGO\b": "ALGO",
    r"\$ALPHA\b": "ALPHA",
    r"\$ANKR\b": "ANKR",
    r"\$AXS\b": "AXS",
    r"\$BAKE\b": "BAKE",
    r"\$BAND\b": "BAND",
    r"\$BAT\b": "BAT",
    r"\$BEL\b": "BEL",
    r"\$BNB\b": "BNB",
    r"\$BCH\b": "BCH",
    r"\$BTC\b": "BTC",
    r"\$BTS\b": "BTS",
    r"\$BLZ\b": "BLZ",
    r"\$BZRX\b": "BZRX",
    r"\$ADA\b": "ADA",
    r"\$CELR\b": "CELR",
    r"\$CTK\b": "CTK",
    r"\$LINK\b": "LINK",
    r"\$CHZ\b": "CHZ",
    r"\$CHR\b": "CHR",
    r"\$COTI\b": "COTI",
    r"\$CRV\b": "CRV",
    r"\$MANA\b": "MANA",
    r"\$YFII\b": "YFII",
    r"\bDOGE\b": "DOGE",
    r"\$EGLD\b": "EGLD",
    r"\$ENJ\b": "ENJ",
    r"\$ETC\b": "ETC",
    r"\$ETH\b": "ETH",
    r"\$FIL\b": "FIL",
    r"\$FLM\b": "FLM",
    r"\$HBAR\b": "HBAR",
    r"\$ZEN\b": "ZEN",
    r"\$ICX\b": "ICX",
    r"\$RLC\b": "RLC",
    r"\$ICP\b": "ICP",
    r"\$IOST\b": "IOST",
    r"\$IOTA\b": "IOTA",
    r"\$KAVA\b": "KAVA",
    r"\$KSM\b": "KSM",
    r"\$KNC\b": "KNC",
    r"\$LINA\b": "LINA",
    r"\$LTC\b": "LTC",
    r"\$LIT\b": "LIT",
    r"\$LRC\b": "LRC",
    r"\$XMR\b": "XMR",
    r"\$ALICE\b": "ALICE",
    r"\$NEAR\b": "NEAR",
    r"\$XEM\b": "XEM",
    r"\$OCEAN\b": "OCEAN",
    r"\$OMG\b": "OMG",
    r"\$OGN\b": "OGN",
    r"\$DOT\b": "DOT",
    r"\$QTUM\b": "QTUM",
    r"\$RVN\b": "RVN",
    r"\$SFP\b": "SFP",
    r"\$SC\b": "SC",
    r"\$SKL\b": "SKL",
    r"\$SOL\b": "SOL",
    r"\$STORJ\b": "STORJ",
    r"\$STMX\b": "STMX",
    r"\$SUSHI\b": "SUSHI",
    r"\$SNX\b": "SNX",
    r"\$TRB\b": "TRB",
    r"\$XTZ\b": "XTZ",
    r"\$RUNE\b": "RUNE",
    r"\$TOMO\b": "TOMO",
    r"\$TRX\b": "TRX",
    r"\$UNFI\b": "UNFI",
    r"\$UNI\b": "UNI",
    r"\$VET\b": "VET",
    r"\$WAVES\b": "WAVES",
    r"\$XRP\b": "XRP",
    r"\$YFI\b": "YFI",
    r"\$ZEC\b": "ZEC",
    r"\$ZIL\b": "ZIL",
}


def match_text(text_raw):
    text = unidecode(text_raw)
    for word in blacklist:
        if re.search(word, text, flags=re.I) is not None:
            return None  # "SPAM"
    ticker_found = None
    for re_pattern, ticker in ticker_list.items():
        if re.search(re_pattern, text, flags=re.I) is not None:
            if ticker_found is not None and ticker_found != ticker:
                return None  # "MANY"
            else:
                ticker_found = ticker
    return ticker_found


def get_tweet_timestamp(tid):
    offset = 1288834974657
    tstamp = (tid >> 22) + offset
    # utcdttime = datetime.utcfromtimestamp(tstamp / 1000)
    # print(str(tid) + " : " + str(tstamp) + " => " + str(utcdttime))
    return tstamp


def get_time_difference(tweet2, tweet1):
    return get_tweet_timestamp(tweet2['id']) - get_tweet_timestamp(tweet1['id'])


def get_historical_price(time_s, ticker):
    if ticker == "MANY" or ticker == "SPAM":
        return 0, 0, 0, 0, 0
    old_stdout = sys.stdout  # backup current stdout
    sys.stdout = open(os.devnull, "w")

    request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                   secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")

    result = request_client.get_candlestick_data(symbol=ticker + "USDT", interval=CandlestickInterval.MIN1,
                                                 startTime=time_s * 1000 - 60000, limit=30)

    sys.stdout = old_stdout  # reset old stdout
    close_price = Decimal(result[1].close)
    open_price = Decimal(result[0].open)
    price_increase_2min = (Decimal(result[1].close) - Decimal(result[0].open)) / Decimal(result[0].open)
    price_increase_10min = (Decimal(result[9].close) - Decimal(result[0].open)) / Decimal(result[0].open)
    price_increase_30min = (Decimal(result[29].close) - Decimal(result[0].open)) / Decimal(result[0].open)
    #  print(f"Price increase: {price_increase:.4f}")
    return open_price, close_price, price_increase_2min, price_increase_10min, price_increase_30min


def get_historical_price_2min(time_s, ticker):
    old_stdout = sys.stdout  # backup current stdout
    sys.stdout = open(os.devnull, "w")
    request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                   secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")

    result = request_client.get_candlestick_data(symbol=ticker + "USDT", interval=CandlestickInterval.MIN1,
                                                 startTime=time_s * 1000 - 60000, limit=2)

    sys.stdout = old_stdout  # reset old stdout
    close_price = Decimal(result[1].close)
    open_price = Decimal(result[0].open)
    price_increase = (Decimal(result[1].close) - Decimal(result[0].open)) / Decimal(result[0].open)
    #  print(f"Price increase: {price_increase:.4f}")
    return open_price, close_price, price_increase


def update_price_diff(list_of_tweets):
    for tweet in list_of_tweets:
        timestamp = get_tweet_timestamp(tweet['id']) / 1000
        age_s = time.time() - timestamp
        if (age_s > MAX_AGE_S) and 'price_diff_2min' not in tweet:
            historical_price = get_historical_price(timestamp, tweet['ticker'])

            tweet['price_diff_2min'] = round(historical_price[2] * 100, 2)
            tweet['price_diff_10min'] = round(historical_price[3] * 100, 2)
            tweet['price_diff_30min'] = round(historical_price[4] * 100, 2)
            with open("top_tweets.txt", "a", encoding="utf-8") as f:
                f.write(f"{tweet['ticker']:>6}, "
                        f"02min_d%: {tweet['price_diff_2min']:+4.2f}%, "
                        f"10min_d%: {tweet['price_diff_10min']:+4.2f}%, "
                        f"30min_d%: {tweet['price_diff_30min']:+4.2f}%, "
                        f"traded: {tweet['traded']:>1}, "
                        f"Scr: {tweet['sentiment_score']:+3.2f}, "
                        f"Mag: {tweet['sentiment_magnitude']:4.2f}, "
                        f"Max RT/s: {tweet['max_rt_s']:4.2f}, "
                        f"{tweet['created_at']}, "
                        f"{tweet['text_line']}, "
                        f"{tweet['link']}, "
                        f"History [s,RT/s]: {tweet['history']}, "
                        "\n"
                        )
                f.close()


client = language_v1.LanguageServiceClient()
top_tweets = []
expr = []

for re_pattern, ticker in ticker_list.items():
    expression = re_pattern.replace(r"\b", "")
    expression = expression.replace(r"\$", "$")
    expr.append(expression)
while True:

    print('Start')
    try:
        tweets = api.GetStreamFilter(track=expr)
        request_client = RequestClient(api_key="kcFnjpVAzw4F9PNXs2I8zvSW0vAnmhOmmXHDIhBPEHdCg0Ye9ZzX4VdcSevouMcR",
                                       secret_key="MEWSAyyWn8xgzffUa4YzdsXpwyWO6wDMabg3IPMs8Xq19qeroKsylFqNoQek7sy2")
        for tweet in tweets:
            if 'retweeted_status' in tweet:
                new_top_tweet = tweet['retweeted_status']
                age_s = get_time_difference(tweet, new_top_tweet) / 1000
                if age_s < MIN_AGE_S: continue
                if age_s > MAX_AGE_S: continue

                if new_top_tweet['truncated']:
                    text = new_top_tweet['extended_tweet']['full_text']
                else:
                    text = new_top_tweet['text']
                ticker = match_text(text)
                if ticker is None: continue

                retweets_per_s = round(new_top_tweet['retweet_count'] / age_s, 2)
                if (ticker == "BTC") or (ticker == "DOGE"):
                    if retweets_per_s < MIN_RETWEETS_PER_S_MAIN: continue
                else:
                    if retweets_per_s < MIN_RETWEETS_PER_S_ALT: continue

                text = text.replace('\n', '//')

                update_price_diff(top_tweets)

                first_time = True
                trade = False
                if age_s <= MAX_AGE_S_TO_BUY:
                    if (ticker == "BTC") or (ticker == "DOGE"):
                        if retweets_per_s >= MIN_RETWEETS_PER_S_MAIN_TO_BUY:
                            trade = True
                    elif ticker == "MANY" or ticker == "SPAM":
                        trade = False
                    else:
                        if retweets_per_s >= MIN_RETWEETS_PER_S_ALT_TO_BUY:
                            trade = True

                for top_tweet in top_tweets:
                    if top_tweet['id'] == new_top_tweet['id']:
                        first_time = False
                        if top_tweet['traded']:
                            trade = False

                        if trade:
                            top_tweet['traded'] = True

                        top_tweet['history'].append([age_s, retweets_per_s])
                        top_tweet['max_rt_s'] = max(top_tweet['max_rt_s'], retweets_per_s)
                        break

                if trade:
                    place_order_available(ticker, ORDER_BALANCE_FRACTION, TRAILING_STOP_PERCENT)
                    new_top_tweet['traded'] = True
                else:
                    new_top_tweet['traded'] = False

                if first_time:
                    top_tweets.append(new_top_tweet)

                    document = language_v1.Document(content=text, type_=language_v1.Document.Type.PLAIN_TEXT)
                    # Detects the sentiment of the text
                    sentiment = client.analyze_sentiment(request={'document': document}).document_sentiment

                    link = f"https://twitter.com/{new_top_tweet['user']['screen_name']}/status/{new_top_tweet['id']}"
                    new_top_tweet['link'] = link
                    new_top_tweet['ticker'] = ticker
                    new_top_tweet['history'] = [[age_s, retweets_per_s]]
                    new_top_tweet['max_rt_s'] = retweets_per_s
                    new_top_tweet['time_added'] = time.time()
                    new_top_tweet['sentiment_score'] = round(sentiment.score, 2)
                    new_top_tweet['sentiment_magnitude'] = round(sentiment.magnitude, 2)
                    new_top_tweet['text_line'] = text
                    print(f"{ticker:>6}",
                          f"Scr: {new_top_tweet['sentiment_score']:+3.2f}, "
                          f"Mag: {new_top_tweet['sentiment_magnitude']}, "
                          f"Age: {age_s:7.2f}, "
                          f"RT/s: {retweets_per_s:4.2f}",
                          new_top_tweet['created_at'], text, link)



    except Exception as ex:
        print(ex, 'Error')
        time.sleep(5)
    continue

    #
